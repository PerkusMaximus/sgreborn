# Development Progress #
* **Structure Core Functionality**
    * **Design Asynchronous Map Loading System [FAWE]**
    * **Design Soundex Phonectif Algorithm**
## Module Library ##

Module  | Status
------------- | -------------
Front End Design | **Not Started**
Map Loading | **Not Started**
Sound Manager | **Not Started**
## Version Compatability ##

Version  | Status
------------- | -------------
1.12.X | **Planned**
1.11.X | **Planned**
1.10.X | **Planned**
1.9.X | **Planned**
1.8.X | **Planned**
1.7.X | **Planned**